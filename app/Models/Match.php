<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{

    public function resultats()
    {
        return $this->hasMany(Resultat::class);
    }
}
