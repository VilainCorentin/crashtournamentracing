<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AnnouncementMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        ini_set('max_execution_time', '300');
        $user = $this->user;
        return $this->subject('Une session de matchmaking sur CrashTournamentRacing ? [Erratum]')->view('Mail.annoncement', compact('user'));
    }
}
