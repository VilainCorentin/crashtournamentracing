<?php

namespace App\Http\Controllers;

use App\Http\Requests\VideoRequest;
use App\Models\User;
use App\Models\Video;
use App\Models\Video_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::query()
            ->orderBy('like', 'desc')
            ->limit(15)->get();

        foreach ($videos as &$video){
            $pos = strpos($video->url, '=') + 1;
            $id = substr($video->url, $pos);
            $video->video_url = 'https://www.youtube.com/embed/' . $id  . '?controls=0';
            $user = User::find($video->user_id);
            $video->user_name = $user->name;
            $video->user_avatar = $user->avatar;
        }

        $title = 'Top 15 des meilleurs vidéos';

        return view('classement.video', compact('videos', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(VideoRequest $request)
    {

        if($request->user()->id != Auth::user()->id){
            return back()->with ('error', __ ("Looool !!!!"));
        }
        elseif (count($request->user()->videos()->get()) >= 3){
            return back()->with ('error', __ ("La limite de vidéos est de 3."));
        }

        $video = new Video();

        $video->url = $request->video;
        $video->description = $request->description;

        $request->user()->videos()->save($video);

        return back()->with ('ok', __ ("La video a bien été ajoutée."));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Video $video)
    {
        $video->delete ();
        return back()->with ('ok', __ ("La video a bien été supprimée."));
    }

    public function like(Video $video){

        $already_like = Video_user::query()->where('user_id', '=', Auth::user()->id)
        ->where('video_id', '=', $video->id)
        ->get();

        if (!$already_like->isEmpty()){
            $video->like--;
            $video->save();

            $already_like[0]->delete();

            return back()->with ('ok', __ ("Vous n'aimez plus la vidéo."));
        }else{
            $video->like++;
            $video->save();

            $new_video_like = new Video_user();
            $new_video_like->user_id = Auth::user()->id;
            $new_video_like->video_id = $video->id;
            $new_video_like->save();

            return back()->with ('ok', __ ("Vous aimez la vidéo."));
        }

    }

    public function videoEdit(User $user)
    {
        $this->authorize('manage', $user);
        $videos = $user->videos()->get();
        return view('user.video', compact('user', 'videos'));
    }

    public function lastVideo()
    {
        $videos = Video::query()
            ->orderBy('created_at', 'desc')
            ->limit(15)->get();

        foreach ($videos as &$video){
            $pos = strpos($video->url, '=') + 1;
            $id = substr($video->url, $pos);
            $video->video_url = 'https://www.youtube.com/embed/' . $id  . '?controls=0';
            $user = User::find($video->user_id);
            $video->user_name = $user->name;
            $video->user_avatar = $user->avatar;
        }

        $title = 'Les 15 dernières vidéos';

        return view('classement.video', compact('videos', 'title'));
    }
}
