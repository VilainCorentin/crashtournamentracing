<?php

namespace App\Http\Controllers;

use App\Models\DisponiblePlayer;
use App\Models\Match;
use App\Models\Resultat;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatController extends Controller
{
    public function statPlayers()
    {

        DisponiblePlayer::query()
            ->join('users', 'users.id', '=', 'disponible_players.user_id')
            ->where(
                'disponible_players.updated_at',
                '<',
                Carbon::now()->subSeconds(15)->toDateTimeString()
            )
            ->forceDelete();

        $count_ps4 = DisponiblePlayer::query()
            ->join('users', 'users.id', '=', 'disponible_players.user_id')
            ->where('users.plateforme', '=', 'Playstation 4')
            ->count();

        $count_xbox = DisponiblePlayer::query()
            ->join('users', 'users.id', '=', 'disponible_players.user_id')
            ->where('users.plateforme', '=', 'Xbox One')
            ->count();

        $count_switch = DisponiblePlayer::query()
            ->join('users', 'users.id', '=', 'disponible_players.user_id')
            ->where('users.plateforme', '=', 'Switch')
            ->count();

        $count_match_going = Resultat::query()
            ->whereNull('resultat')
            ->count();

        $count_match_going = round(($count_match_going / 2), 0, PHP_ROUND_HALF_DOWN);

        $count_match_today = Match::whereDate('created_at', Carbon::today())->count();

        return json_encode([
            'count_ps4' => $count_ps4,
            'count_xbox' => $count_xbox,
            'count_switch' => $count_switch,
            'count_match_going' => $count_match_going,
            'count_match_today' => $count_match_today
        ]);
    }
}
