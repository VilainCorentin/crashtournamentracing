<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Resultat;
use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Illuminate\Support\Facades\Config;

class UserController extends Controller
{

    public function show(User $user)
    {
        $id_user = $user->id;

        $result = DB::select(
            DB::raw("SELECT rank FROM (
                                        SELECT @rn:=@rn+1 AS rank, id, points, plateforme
                                        FROM (
                                          SELECT id, points, plateforme
                                          FROM users
                                          where plateforme = :plateforme1
                                          ORDER BY points DESC
                                        ) t1, (SELECT @rn:=0) t2) ranking
                                        WHERE id = :id_user
                                        AND plateforme = :plateforme2;"),
            [
                'id_user' => $id_user,
                'plateforme1' => $user->plateforme,
                'plateforme2' => $user->plateforme,
            ]
    );

        $user->rank_classement = $result[0]->rank;
        $user->total_player = User::query()
            ->where('plateforme', '=', $user->plateforme)
            ->count();

        $all_matchs = [];

        $resultats = $user->resultats()
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();

        foreach ($resultats as &$resultat){

            $match_array = [];

            $match_adv = $resultat->match()->get();
            $resultat_adv = $match_adv[0]->resultats()
                ->where('user_id', '!=', $id_user)
                ->get();

            if($resultat->resultat == 1){
                $match_result = __('Victoire');
            }elseif ($resultat->resultat == 0 && !is_null($resultat->resultat)){
                $match_result = __('Défaite');
            }elseif ($resultat->resultat == 2){
                $match_result = __('Égalité');
            }else{
                $match_result = __('En cours');
            }

            $adversary = $resultat_adv[0]->user()->get();

            $match_array['result_int'] = $resultat->resultat;
            $match_array['result'] = $match_result;
            $match_array['adversary'] = $adversary[0]->plateforme_name;
            $match_array['map'] = __(Config::get('constants.circuits')[$match_adv[0]->map]);
            $match_array['date'] = $match_adv[0]->created_at->format('d/m/Y');

            $all_matchs[] = $match_array;
        }

        $videos = $user->videos()->get();

        foreach ($videos as &$video){
            $pos = strpos($video->url, '=') + 1;
            $id = substr($video->url, $pos);
            $video->video_url = 'https://www.youtube.com/embed/' . $id  . '?controls=0';
        }

        return view('user.index', compact('user', 'all_matchs', 'videos'));
    }

    public function edit(User $user)
    {
        $this->authorize('manage', $user);
        $videos = $user->videos()->get();
        return view('user.edit', compact('user', 'videos'));
    }

    public function update(UserRequest $request, User $user)
    {
        $user->bio = $request->bio;
        $user->update($request->all());
        return redirect()->route('user.edit', $user->id)->with('ok', __("L'utilisateur a bien été modifié"));
    }

    public function language(String $locale)
    {
        $locale = in_array($locale, config('app.locales')) ? $locale : config('app.fallback_locale');
        session(['locale' => $locale]);
        return back();
    }
}
