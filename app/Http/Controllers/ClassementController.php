<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class ClassementController extends Controller
{
    public function general($platform)
    {
        $users = User::query()->whereHas('resultats')
            ->where('plateforme', '=', $platform)
            ->orderBy('points', 'desc')
            ->limit(50)->get();
        return view('classement.general', compact('users', 'platform'));
    }

    public function generalText($plateform)
    {
        $users = User::query()->whereHas('resultats')
        ->where('plateforme', '=', $plateform)
        ->orderBy('points', 'desc')
        ->limit(50)
        ->get();

        $textClassement = "";
        $position = 1;

        foreach ($users as $user){
            $textClassement .= ($position . ' | ' . $user->name . ' | ' . $user->points . '</br>');
            $position++;
        }

        return $textClassement;
    }

    public function generalDiscord($plateform)
    {
        $users = User::query()->whereHas('resultats')
            ->where('plateforme', '=', $plateform)
            ->orderBy('points', 'asc')
            ->limit(50)
            ->get();

        date_default_timezone_set('Europe/Paris');

        $plateform_string = "";

        if($plateform == "Playstation 4"){
            $plateform_string = "PS4";
        }
        elseif ($plateform == "Switch")
        {
            $plateform_string = strtoupper($plateform);
        }
        elseif ($plateform == "Xbox One"){
            $plateform_string = "XBOX";
        }

        $textClassement = "";
        $position = count($users);

        foreach ($users as $user){
            if ($position == 1){
                $textClassement .= ':first_place:';
            }
            elseif ($position == 2){
                $textClassement .= ':second_place:';
            }
            elseif ($position == 3){
                $textClassement .= ':third_place:';
            }
            else{
                $textClassement .= $position;
            }

            $textClassement .= (' | ' . $user->name . ' | ' . $user->points . '</br>');

            $position--;
        }

        $textClassement .= "</br>:bar_chart: **Classement Ligue " . $plateform_string . " - 1 vs 1** :bar_chart: _" . date("d/m/Y G:i") . "_";

        date_default_timezone_set('UTC');

        return $textClassement;
    }
}
