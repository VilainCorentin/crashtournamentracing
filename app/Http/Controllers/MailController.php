<?php

namespace App\Http\Controllers;

use App\Mail\AnnouncementMail;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function annoncement(){
        $users = \App\Models\User::all();
        foreach($users as $user)
        {
            Mail::to($user->email)->send(new AnnouncementMail($user));
        }
    }
}
