<?php

namespace App\Http\Controllers;

use App\Models\Match;
use App\Models\Resultat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller
{
    public function home(){

        $all_matchs = [];

        $matches = Match::query()
            ->join('resultats', 'matches.id', '=', 'resultats.match_id')
            ->where('resultat', '!=', '2')
            ->whereNotNull('resultat')
            ->limit(10)
            ->orderBy('matches.id', 'desc')
            ->groupBy('matches.id')
            ->select('matches.id')
            ->get();

        foreach ($matches as &$match_id){

            $id = $match_id->id;

            $match = Match::where('id', $id)->first();

            $resultats = $match->resultats()->get();

            $user1 = $resultats[0]->user()->get()[0];
            $user2 = $resultats[1]->user()->get()[0];

            $resultat = $resultats[0]->resultat;

            if($resultat == 1){
                $match_result = __('Victoire');
            }elseif ($resultat == 0){
                $match_result = __('Défaite');
            }elseif ($resultat == 2){
                $match_result = __('Égalité');
            }else{
                $match_result = __('En cours');
            }

            $date = $match->created_at->format('d/m/Y');
            $plateforme = $user1->plateforme;
            $name1 = $user1->name;
            $name2 = $user2->name;
            $map = __(Config::get('constants.circuits')[$match->map]);

            $match_array = [];

            $match_array['result_int'] = $resultat;
            $match_array['result'] = $match_result;
            $match_array['name1'] = $name1;
            $match_array['name2'] = $name2;
            $match_array['map'] = $map;
            $match_array['date'] = $date;
            $match_array['plateforme'] = $plateforme;

            $all_matchs[] = $match_array;
        }

        $users = User::query()->whereHas('resultats')
            ->orderBy('points', 'desc')
            ->limit(10)->get();

        return view('home', compact( 'all_matchs', 'users'));
    }
}
