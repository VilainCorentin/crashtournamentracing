<?php

namespace App\Http\Controllers;

use App\Models\DisponiblePlayer;
use App\Models\Match;
use App\Models\Resultat;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class MatchmakingController extends Controller
{

    public const WIN_POINTS = 10;
    public const LOSE_POINTS = 5;
    public const CHEAT_LOSE_POINTS = 10;
    public const CHEAT_LOSE_BEHAVIOR = 5;
    public const WIN_BEHAVIOR = 2;
    public const EQUALITY_LOSE = 5;
    public const BEHAVIOR_RANGE = 20;

    public const RANGE_PTS = 45;
    public const VAR_RANGE_PTS = 5;

    public const MSG_GOOD = 'good';
    public const MSG_BAD = 'bad';

    public function __construct()
    {
        $this->middleware('ajax')->only('researchMaking');
    }

    public function researchMaking(User $user, $iter)
    {

        // Si c'est bien le user qui a envoyé la requete
        $this->authorize('manage', $user);

        $range_pts = self::RANGE_PTS + (self::VAR_RANGE_PTS * $iter);

        $msg_rep = 0;

        // On check si on a un match ou non en cours
        $resultat = Resultat::where('user_id', '=', $user->id)
            ->whereNull('resultat')->get();

        if ($resultat->count()) {
            $resultat = $resultat[0];

            //Return la vue du match
            $match = $resultat->match()->get()[0];
            $resultat_join = $match->resultats()->where('user_id', '!=', $user->id)->get()[0];

            if (!is_null($resultat_join->resultat)) {
                $msg_rep = 1;
            }

            return json_encode([
                'user' => $resultat->user_id,
                'user_adv' => $resultat_join->user_id,
                'match' => $match->id,
                'msg' => $msg_rep
                ]);
        }

        // Mise à jour de la date de dispo
        DisponiblePlayer::firstOrNew(['user_id' => $user->id])->touch();

        // Suppresion de ceux qui ne recherche plus de match
        DisponiblePlayer::when($user->id, function ($query, $role) {
            return $query->where('user_id', '!=', $role);
        })
            ->join('users', 'users.id', '=', 'disponible_players.user_id')
            ->whereBetween('points', [($user->points - $range_pts), ($user->points + $range_pts)])
            ->where(
                'disponible_players.updated_at',
                '<',
                Carbon::now()->subSeconds(15)->toDateTimeString()
            )
            ->forceDelete();

        // Recherche de joueurs potentiels
        $users_ad = DisponiblePlayer::when($user->id, function ($query, $role) {
            return $query->where('user_id', '!=', $role);
        })
            ->join('users', 'users.id', '=', 'disponible_players.user_id')
            ->whereBetween('points', [($user->points - $range_pts), ($user->points + $range_pts)])
            ->where('users.plateforme', '=', $user->plateforme)
            ->select('users.id')
            ->limit(1)
            ->get();

        // Si on a trouvé un joueur
        if ($users_ad->count()) {
            $users_ad = User::find($users_ad[0]->id);

            // On supprime leurs dispo dans le matchmaking
            DisponiblePlayer::whereIn('user_id', [$user->id, $users_ad->id])
                ->forceDelete();

            // Création du match
            $match = new Match();

            //Génération de la map
            $id_map = rand(0, count(Config::get('constants.circuits')) - 1);
            $match->map = $id_map;
            $match->save();

            // Creation des resultats pour les deux joueurs (creer une fonction lol)
            $resultat_main = new Resultat();
            $resultat_adv = new Resultat();

            $resultat_main->user_id = $user->id;
            $resultat_adv->user_id = $users_ad->id;

            $resultat_main->match_id = $match->id;
            $resultat_adv->match_id = $match->id;

            $resultat_main->save();
            $resultat_adv->save();

            //Return la vue du match
            return json_encode([
                'user' => $user->id,
                'user_adv' => $users_ad->id,
                'match' => $match->id,
                'msg' => $msg_rep
            ]);
        }

        // Si on a rien trouvé, on l'indique à Ajax
        return null;
    }

    public function displayMatch(User $user_main, User $user_adv, Match $match, $msg)
    {
        $match->map_string = __(Config::get('constants.circuits')[$match->map]);
        return view('matchmaking.match', compact('user_main', 'user_adv', 'match', 'msg'));
    }

    private function checkWhoIsTheCheater($user, $user_adv, $resultat, $resultat_adv)
    {

        if ($user->behavior > $user_adv->behavior + self::BEHAVIOR_RANGE) {
            $user->points -= self::CHEAT_LOSE_POINTS;
            $user->behavior += self::CHEAT_LOSE_BEHAVIOR;
            $user->save();

            $user_adv->points += self::WIN_POINTS;
            $user_adv->save();

            $resultat->resultat = 0;
            $resultat->save();

            $resultat_adv->resultat = 1;
            $resultat_adv->save();

            return redirect('/')->with('bad', __('matchmaking.MSG_BEHAVIOR_BAD'));
        } elseif ($user_adv->behavior > $user->behavior + self::BEHAVIOR_RANGE) {
            $user->points += self::WIN_POINTS;
            $user->save();

            $user_adv->points -= self::CHEAT_LOSE_POINTS;
            $user_adv->behavior += self::CHEAT_LOSE_BEHAVIOR;
            $user_adv->save();

            $resultat->resultat = 1;
            $resultat->save();

            $resultat_adv->resultat = 0;
            $resultat_adv->save();

            return redirect('/')->with('good', __('matchmaking.MSG_SAVE_BEHAVIOR'));
        } else {
            $user->points -= self::EQUALITY_LOSE;
            $user->behavior += self::EQUALITY_LOSE;
            $user->save();

            $user_adv->points -= self::EQUALITY_LOSE;
            $user_adv->behavior += self::EQUALITY_LOSE;
            $user_adv->save();

            $resultat->resultat = 0;
            $resultat->save();

            $resultat_adv->resultat = 0;
            $resultat_adv->save();

            return redirect('/')->with('bad', __('matchmaking.MSG_SAME_BEHAVIOR'));
        }
    }

    public function validateMatch(User $user, Match $match, $result)
    {

        $this->authorize('manage', $user);

        $msg_header = '';
        $msg_body = '';

        $resultat = $match->resultats()->where('user_id', '=', $user->id)->get()[0];

        if (!is_null($resultat->resultat)) {
            return redirect('/');
        }

        $resultat_adv = $match->resultats()->where('user_id', '!=', $user->id)->get()[0];
        $user_adv = User::find($resultat_adv->user_id);

        if (!is_null($resultat_adv->resultat)) {

            if ($resultat_adv->resultat == $result) {
                //Cas match null
                if ($result == 2) {
                    if ($user->behavior > 0) {
                        $user->behavior -= self::WIN_BEHAVIOR;
                    }

                    if ($user_adv->behavior > 0) {
                        $user_adv->behavior -= self::WIN_BEHAVIOR;
                    }

                    $user_adv->save();
                    $user->save();

                    $resultat->resultat = 2;
                    $resultat->save();

                    return redirect('/')->with('good', __('Match nul mais vous gagnez de la réputation.'));
                }

                return $this->checkWhoIsTheCheater($user, $user_adv, $resultat, $resultat_adv);
            } else {
                // Cas de tentative de triche avec un match nul
                if ($resultat_adv->resultat == 2 || $result == 2) {
                    return $this->checkWhoIsTheCheater($user, $user_adv, $resultat, $resultat_adv);
                }

                $resultat->resultat = $result;
                $resultat->save();

                //Si je n'ai pas plus de 3 ranks de diff
                if(!($user_adv->rank - $user->rank < -3 && $result == 1)){

                    $supRank = -1;
                    $bonusRank = 5;

                    if($user_adv->rank - $user->rank < 0)
                    {
                        $supRank = 1;
                        $bonusRank = 2;
                    }
                    elseif ($user_adv->rank == $user->rank)
                    {
                        $supRank = 0;
                        $bonusRank = 0;
                    }

                    $rankDiff = abs($user->rank - $user_adv->rank) * $bonusRank;

                    $serieBonus = $user->serie;

                    if($serieBonus >= 9)
                    {
                        $serieBonus = 9;
                    }
                    elseif ($serieBonus < 0){
                        $serieBonus = 0;
                    }

                    $ptsFinal = 0;

                    if ($result == 1) {

                        if($supRank == 1)
                        {
                            $ptsFinal = (self::WIN_POINTS - $rankDiff);
                            $user->points += $ptsFinal;
                        }
                        elseif ($supRank == -1)
                        {
                            $ptsFinal = (self::WIN_POINTS + $rankDiff);
                            $user->points += $ptsFinal;
                        }
                        else
                        {
                            $ptsFinal = (self::WIN_POINTS + $serieBonus);
                            $user->points += $ptsFinal;
                        }

                        if($user->serie < 0)
                        {
                            $user->serie = 0;
                        }
                        else
                        {
                            $user->serie++;
                        }

                        $user->save();

                        $topUser = User::query()->where('rank', '=', $user->rank)
                            ->where('plateforme', '=', $user->plateforme)
                            ->orderBy('points', 'desc')
                            ->limit(1)
                            ->get()[0];

                        if($topUser->id == $user->id){
                            if($user->rank != 6){
                                $user->rank++;
                            }
                        }

                        $msg_header = self::MSG_GOOD;
                        $msg_body = __('Bravo ! Vous avez gagné ') . $ptsFinal . __(' points et de la réputation.');

                    } elseif ($result == 0) {

                        if($supRank == 1)
                        {
                            $ptsFinal = (self::LOSE_POINTS + $rankDiff);
                            $user->points -= $ptsFinal;
                        }
                        elseif ($supRank == -1)
                        {
                            $ptsFinal = ((self::LOSE_POINTS - $rankDiff) + 5);

                            if($ptsFinal < 0)
                            {
                                $ptsFinal = 1;
                            }

                            $user->points -= $ptsFinal;
                        }
                        else
                        {
                            $ptsFinal = self::LOSE_POINTS;
                            $user->points -= $ptsFinal;
                        }

                        if($user->serie > 0)
                        {
                            $user->serie = 0;
                        }
                        else
                        {
                            $user->serie--;
                        }

                        $user->save();

                        $lessUser = User::query()->where('rank', '=', $user->rank)
                            ->where('plateforme', '=', $user->plateforme)
                            ->orderBy('points', 'asc')
                            ->limit(1)
                            ->get()[0];

                        if ($lessUser->id == $user->id)
                        {
                            if($user->rank != 0){
                                $user->rank--;
                            }
                        }

                        $msg_header = self::MSG_BAD;
                        $msg_body = __('Mince ! Vous avez perdu ') . self::LOSE_POINTS . __(' points mais gagné de la réputation :).');

                    }

                    if($user->points < 0){
                        $user->points = 0;
                    }

                }

                if ($user->behavior > 0) {
                    $user->behavior -= self::WIN_BEHAVIOR;
                }
                if ($user_adv->behavior > 0) {
                    $user_adv->behavior -= self::WIN_BEHAVIOR;
                }

                $user_adv->save();
                $user->save();

                return redirect('/')->with($msg_header, $msg_body);
            }
        } else {
            $resultat->resultat = $result;
            $resultat->save();

            //Si je n'ai pas plus de 3 ranks de diff
            if(!($user_adv->rank - $user->rank < -3 && $result == 1)){

                $supRank = -1;
                $bonusRank = 5;

                if($user_adv->rank - $user->rank < 0)
                {
                    $supRank = 1;
                    $bonusRank = 2;
                }
                elseif ($user_adv->rank == $user->rank)
                {
                    $supRank = 0;
                    $bonusRank = 0;
                }

                $rankDiff = abs($user->rank - $user_adv->rank) * $bonusRank;

                $serieBonus = $user->serie;

                if($serieBonus >= 9)
                {
                    $serieBonus = 9;
                }
                elseif ($serieBonus < 0){
                    $serieBonus = 0;
                }

                if ($result == 1) {

                    if($supRank == 1)
                    {
                        $user->points += (self::WIN_POINTS - $rankDiff);
                    }
                    elseif ($supRank == -1)
                    {
                        $user->points += (self::WIN_POINTS + $rankDiff);
                    }
                    else
                    {
                        $user->points += (self::WIN_POINTS + $serieBonus);
                    }

                    if($user->serie < 0)
                    {
                        $user->serie = 0;
                    }
                    else
                    {
                        $user->serie++;
                    }

                    $user->save();

                    $topUser = User::query()->where('rank', '=', $user->rank)
                        ->where('plateforme', '=', $user->plateforme)
                        ->orderBy('points', 'desc')
                        ->limit(1)
                        ->get()[0];

                    if($topUser->id == $user->id){
                        if($user->rank != 6){
                            $user->rank++;
                        }
                    }

                } elseif ($result == 0) {

                    if($supRank == 1)
                    {
                        $user->points -= (self::LOSE_POINTS + $rankDiff);
                    }
                    elseif ($supRank == -1)
                    {

                        $ptsFinal = ((self::LOSE_POINTS - $rankDiff) + 5);

                        if($ptsFinal < 0)
                        {
                            $ptsFinal = 1;
                        }

                        $user->points -= $ptsFinal;
                    }
                    else
                    {
                        $user->points -= self::LOSE_POINTS;
                    }

                    if($user->serie > 0)
                    {
                        $user->serie = 0;
                    }
                    else
                    {
                        $user->serie--;
                    }

                    $user->save();

                    $lessUser = User::query()->where('rank', '=', $user->rank)
                        ->where('plateforme', '=', $user->plateforme)
                        ->orderBy('points', 'asc')
                        ->limit(1)
                        ->get()[0];


                    if ($lessUser->id == $user->id)
                    {
                        if($user->rank != 0){
                            $user->rank--;
                        }
                    }

                }

                if($user->points < 0){
                    $user->points = 0;
                }

                $user->save();
            }

            return redirect('/')->with(self::MSG_GOOD, __('matchmaking.MSG_REGISTER_RESULTAT'));
        }
    }
}
