<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Storage, File};
use Intervention\Image\Facades\Image;
use App\Models\User;

class UploadImagesController extends Controller
{
    private $photos_path;
    private $thumbs_path;
    public function __construct()
    {
        $this->photos_path = public_path('/avatars');
        $this->thumbs_path = public_path('/thumbs');
    }

    public function destroy(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if (!empty($user->avatar)) {
            $file_path = $this->photos_path . '/' . $user->avatar;
            $thumb_file = $this->thumbs_path . '/' . $user->avatar;
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            if (file_exists($thumb_file)) {
                unlink($thumb_file);
            }

            $user->avatar = null;
            $user->save();
        }
    }

    public function getServerImages(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if(!is_null($user->avatar)){
            $imageAnswer[] = [
                'original' => 'avatar',
                'server' => $user->avatar,
                'size' => File::size($this->photos_path . '/' . $user->filename),
            ];

            return response()->json([
                'image' => $imageAnswer
            ]);
        }
    }

    /**
     * Saving images uploaded through XHR Request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $photos = $request->file('file');

        if (!is_array($photos)) {
            $photos = [$photos];
        }
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path);
        }
        if (!is_dir($this->thumbs_path)) {
            mkdir($this->thumbs_path);
        }

        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            $name = str_random(30);
            $save_name = $name . '.' . $photo->getClientOriginalExtension();
            Image::make($photo)
                ->resize(200, null, function ($constraints) {
                    $constraints->aspectRatio();
                })
                ->save($this->photos_path . '/' . $save_name);

            $user = User::find(Auth::user()->id);
            $user->avatar = $save_name;
            $user->save();
        }
    }
}
