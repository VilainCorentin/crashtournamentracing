pipeline {
  agent any
  stages {
    stage('Pre-conditions check') {
      parallel {
        stage('Php') {
          steps {
            sh 'php -v'
          }
        }
        stage('Phpcs') {
          steps {
            sh 'phpcs --version'
          }
        }
        stage('Java') {
          steps {
            sh 'which java'
            sh 'echo $JAVA_HOME'
          }
        }
        stage('Composer') {
          steps {
            sh 'composer install'
          }
        }
        stage('yarn') {
          steps {
            sh 'yarn install'
          }
        }
        stage('Necessary folders') {
          steps {
            sh 'mkdir -p tests'
          }
        }
      }
    }
    stage('PSR2 Test') {
      steps {
        sh 'phpcs'
      }
    }
    stage('chmod envCreator') {
      steps {
        sh 'chmod +x ./envCreator.sh'
      }
    }
    stage('Set env') {
      environment {
        JENKINS_CTR_OVH_DATABASE_HOST  = credentials('jenkins-ctr-ovh-database-host')
        JENKINS_CTR_OVH_DATABASE_PORT  = credentials('jenkins-ctr-ovh-database-port')
        JENKINS_CTR_OVH_DATABASE_DEV_NAME  = credentials('jenkins-ctr-ovh-database-dev-name')
        JENKINS_CTR_OVH_DATABASE_RELEASE_NAME  = credentials('jenkins-ctr-ovh-database-release-name')
        JENKINS_CTR_OVH_DATABASE_MASTER_NAME  = credentials('jenkins-ctr-ovh-database-master-name')
        JENKINS_CTR_OVH_DATABASE_USERNAME  = credentials('jenkins-ctr-ovh-database-username')
        JENKINS_CTR_OVH_DATABASE_PASSWORD  = credentials('jenkins-ctr-ovh-database-password')
      }
      steps {
        script{
            if(env.BRANCH_NAME == "release"){
                stage('release'){
                    sh './envCreator.sh -n "CTR Release" -d true -u https://release.ctr.killian.ovh -a $JENKINS_CTR_OVH_DATABASE_HOST -b $JENKINS_CTR_OVH_DATABASE_PORT -c $JENKINS_CTR_OVH_DATABASE_RELEASE_NAME -e $JENKINS_CTR_OVH_DATABASE_USERNAME -f $JENKINS_CTR_OVH_DATABASE_PASSWORD'
                }
            }else if(env.BRANCH_NAME == "master"){
                stage('master'){
                    sh './envCreator.sh -n "CTR" -d true -u https://ctr.killian.ovh -a $JENKINS_CTR_OVH_DATABASE_HOST -b $JENKINS_CTR_OVH_DATABASE_PORT -c $JENKINS_CTR_OVH_DATABASE_MASTER_NAME -e $JENKINS_CTR_OVH_DATABASE_USERNAME -f $JENKINS_CTR_OVH_DATABASE_PASSWORD'
                }
            } else {
                stage('dev'){
                    sh './envCreator.sh -n "CTR Dev" -d true -u https://dev.ctr.killian.ovh -a $JENKINS_CTR_OVH_DATABASE_HOST -b $JENKINS_CTR_OVH_DATABASE_PORT -c $JENKINS_CTR_OVH_DATABASE_DEV_NAME -e $JENKINS_CTR_OVH_DATABASE_USERNAME -f $JENKINS_CTR_OVH_DATABASE_PASSWORD'
                }
            }
        }
      }
    }
    stage('Generate key') {
      steps {
        sh 'php artisan key:generate'
      }
    }
    stage('Units Test PHP') {
      steps {
        sh './vendor/bin/phpunit --configuration ./phpunit.xml'
      }
    }
    stage('SonarQube Test') {
      steps {
        script {
          scannerHome = tool 'SonarQube'
          withSonarQubeEnv('SonarQube server') {
            sh "${scannerHome}/bin/sonar-scanner"
          }
        }
        
      }
    }
    stage('Quality Gate') {
      steps {
        timeout(time: 1, unit: 'HOURS') {
          script {
            def qg = waitForQualityGate() // Reuse taskId previously collected by withSonarQubeEnv
            if (qg.status != 'OK') {
              error "Pipeline aborted due to quality gate failure: ${qg.status}"
            }
          }
          
        }
        
      }
    }
    stage('Publish reports') {
      steps {
        junit(allowEmptyResults: true, testResults: 'logfile.xml')
        step([
            $class: 'CloverPublisher',
            cloverReportDir: '',
            cloverReportFileName: 'coverage.xml',
            healthyTarget: [methodCoverage: 70, conditionalCoverage: 80, statementCoverage: 80]
        ])
      }
    }
    stage('Docker build') {
      steps {
          script {
              if(env.BRANCH_NAME == "release" || env.BRANCH_NAME == "dev"){
                  stage('nonlatest'){
                    sh "sudo docker build -t killianh/ctr:${GIT_COMMIT} -t killianh/ctr:${BRANCH_NAME} ."
                  }
              }
              if(env.BRANCH_NAME == "master"){
                  stage('latest'){
                    sh "sudo docker build -t killianh/ctr:${GIT_COMMIT} -t killianh/ctr:${BRANCH_NAME}  -t killianh/ctr:latest ."
                  }
              }
          }
      }
    }
    stage('Docker push') {
      steps {
        script {
          if(env.BRANCH_NAME == "release" || env.BRANCH_NAME == "dev" || env.BRANCH_NAME == "master"){
            stage('push'){
              sh "sudo docker push killianh/ctr"
            }
          }
        }
      }
    }
    stage('Docker pull') {
      steps {
        script {
          if(env.BRANCH_NAME == "release" || env.BRANCH_NAME == "dev" || env.BRANCH_NAME == "master"){
            stage('pull'){
              sh "sudo docker pull killianh/ctr:${GIT_COMMIT}"
            }
          }
        }
      }
    }
    stage('Docker stop') {
      steps {
        script {
          if(env.BRANCH_NAME == "release" || env.BRANCH_NAME == "dev" || env.BRANCH_NAME == "master"){
            stage('stop'){
              sh "sudo docker stop ctr${BRANCH_NAME} || true"
            }
          }
        }
      }
    }
    stage('Docker rm') {
        steps {
          script {
            if(env.BRANCH_NAME == "release" || env.BRANCH_NAME == "dev" || env.BRANCH_NAME == "master"){
              stage('rm'){
                sh "sudo docker rm ctr${BRANCH_NAME} || true"
              }
            }
          }
        }
    }
    stage('Docker run') {
        steps {
            script{
                if(env.BRANCH_NAME == "dev"){
                    stage('dev'){
                        sh "sudo docker run -p 10011:9000 -p 10021:6001 --name=ctr${BRANCH_NAME} --restart=always -d killianh/ctr:${GIT_COMMIT}"
                    }
                }
                if(env.BRANCH_NAME == "release"){
                    stage('release'){
                        sh "sudo docker run -p 10012:9000 -p 10022:6001 --name=ctr${BRANCH_NAME} --restart=always -d killianh/ctr:${GIT_COMMIT}"
                    }
                }
                if(env.BRANCH_NAME == "master"){
                    stage('master'){
                        sh "sudo docker run -p 10013:9000 -p 10023:6001 --name=ctr${BRANCH_NAME} --restart=always -d killianh/ctr:${GIT_COMMIT}"
                    }
                }
            }
        }
    }
    stage('Update public folder') {
        steps {
            script{
                if(env.BRANCH_NAME == "dev"){
                    stage('dev'){
                        sh "yarn dev"
                        sh "scp -r ./public root@172.17.0.1:/var/www/ctr/${BRANCH_NAME}"
                    }
                }
                if(env.BRANCH_NAME == "release"){
                    stage('release'){
                        sh "yarn production"
                        sh "scp -r ./public root@172.17.0.1:/var/www/ctr/${BRANCH_NAME}"
                    }
                }
                if(env.BRANCH_NAME == "master"){
                    stage('master'){
                        sh "yarn production"
                        sh "scp -r ./public root@172.17.0.1:/var/www/ctr/${BRANCH_NAME}"
                    }
                }
            }
        }
    }
  }
  post {
	  always {
	    junit 'logfile.xml'
	  }
  }
}