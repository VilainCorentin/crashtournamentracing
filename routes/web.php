<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::name('home')->get('/', 'HomeController@home');

Route::name ('statPlayers')->get('/statPlayers', 'StatController@statPlayers');

Route::name('general')->get('classement/{platform}', 'ClassementController@general');
Route::name('classementApi')->get('classementApi/{platform}/text', 'ClassementController@generalText');
Route::name('classementApi')->get('classementApi/{platform}/discord', 'ClassementController@generalDiscord');

Route::view('/faq', 'faq')->name('faq');

Route::name('language')->get('language/{lang}', 'UserController@language');

//Route::name('annoncement')->get('annoncement', 'MailController@annoncement');

Route::resource('user', 'UserController', [
    'only' => ['show']
]);

Route::resource('video', 'VideoController', [
    'only' => ['index']
]);

Route::name('videoLast')->get('videoLast', 'VideoController@lastVideo');

Route::middleware('auth')->group(function () {

    Route::resource('user', 'UserController', [
        'only' => ['edit', 'update']
    ]);

    Route::view('/research', 'matchmaking.research')->name('research');

    Route::resource('video', 'VideoController', [
        'only' => ['store', 'destroy']
    ]);

    Route::name('videoEdit')->get('videoEdit/{user}', 'VideoController@videoEdit');

    Route::name('videoLike')->get('videolike/{video}', 'VideoController@like');

    Route::name ('researchMatchmaking')->get ('researchMatchmaking/{user}/{iter}', 'MatchmakingController@researchMaking');

    Route::name ('displayMatch')->get ('displayMatch/{user_main}/{user_adv}/{match}/{msg}', 'MatchmakingController@displayMatch');

    Route::name ('validateMatch')->get('validateMatch/{user}/{match}/{result}', 'MatchmakingController@validateMatch');

    Route::middleware('ajax')->group(function () {
        Route::post('images-save', 'UploadImagesController@store')->name('save-images');
        Route::delete('images-delete', 'UploadImagesController@destroy')->name('destroy-images');
        Route::get('images-server','UploadImagesController@getServerImages')->name('server-images');
    });

});
