![alt text](https://gitlab.com/VilainCorentin/crashtournamentracing/raw/master/resources/assets/images/logoCtrvCrash.png "Logo CTR")

# CrashTournamentRacing

CrashTournamentRacing is a matchmaking platform for the game [Crash Team Racing].

The absence of a competitive mode in the game pushed me to develop this platform to offer players a ranking system where everyone can play against an opponent at his own level.

The operation of the platform is really simple:
- Register on the platform
- Indicate on which platform you play and your nickname on it.
- Launch a matchmaking
- Play your match against your opponent (of the same level as you)
- Depending on the result, gain/lose points in the ranking and move up/down in the Elo system.

The platform includes an [Elo rating system] divided into 7 ranks :

![alt text](https://gitlab.com/VilainCorentin/crashtournamentracing/raw/master/public/ranks/0.png "Wumpa")![alt text](https://gitlab.com/VilainCorentin/crashtournamentracing/raw/master/public/ranks/1.png "Cristal")![alt text](https://gitlab.com/VilainCorentin/crashtournamentracing/raw/master/public/ranks/2.png "Gemme")![alt text](https://gitlab.com/VilainCorentin/crashtournamentracing/raw/master/public/ranks/3.png "Gemme Colorée")![alt text](https://gitlab.com/VilainCorentin/crashtournamentracing/raw/master/public/ranks/4.png "Relique Saphir")![alt text](https://gitlab.com/VilainCorentin/crashtournamentracing/raw/master/public/ranks/5.png "Relique Or")![alt text](https://gitlab.com/VilainCorentin/crashtournamentracing/raw/master/public/ranks/6.png "Relique Platine")

### Installation

The project is developed on Laravel.
To launch it at home: 

```sh
$ composer install
$ php artisan key:generate
$ php artisan migrate
$ php artisan serve
```

[Elo rating system]: <https://en.wikipedia.org/wiki/Elo_rating_system>
[Crash Team Racing]: <https://www.crashbandicoot.com/fr/crashteamracing>