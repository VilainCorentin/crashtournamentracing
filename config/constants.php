<?php
return
    [ 'circuits' => [
        'Crique Crash',
        'Grotte Mystère',
        'Circuit Egouts',
        'Tubes Roo',
        'Stade Glissade',
        'Circuit Turbo',
        'Parc Coco',
        'Temple Tigre',
        'Pyramide Papu',
        'Canyon Dingo',
        'Col Polar',
        'Arène Tiny',
        'Mines Dragon',
        'Falaises Glacées',
        'Piste Air',
        'Château Cortex',
        'Labo N. Gin',
        'Station Oxide',
        'Île Infernale',
        'Jungle en folie',
        'Wumpa l\'horloge',
        'Andoïdes',
        'Electrons',
        'Eaux profondes',
        'Foudre',
        'Petit Temple',
        'Gorges du Météore',
        'Ruines de Barin',
        'Hors Temps',
        'Production',
        'Hyperespace',
    ],

    'ranking' => [
        'Wumpa',
        'Cristal',
        'Gemme',
        'Gemme Colorée',
        'Relique Saphir',
        'Relique Or',
        'Relique Platine'
    ]

    ];
