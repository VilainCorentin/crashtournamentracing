window._ = require('lodash');
window.Popper = require('popper.js').default;
window.Dropzone = require('dropzone');
window.Swal = require('sweetalert2');
try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap');
} catch (e) {}
