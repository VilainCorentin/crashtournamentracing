<?php

return [
    'MSG_BEHAVIOR_BAD'             => 'Vous avez rentré le même score que votre adversaire et votre réputation en prend un coup...',
    'MSG_SAVE_BEHAVIOR'             => 'Vous avez rentré le même score que votre adversaire mais votre réputation vous sauve !',
    'MSG_SAME_BEHAVIOR'             => 'Vous avez rentré le même score que votre adversaire et votre réputation est identique à la sienne donc vous perdez tous les deux !',
    'MSG_REGISTER_RESULTAT'             => 'Votre réponse est enregistré mais votre adversaire n\'a pas encore donné la sienne !',
];