<?php

return [
    'MSG_BEHAVIOR_BAD'             => 'You have scored the same as your opponent and your reputation is taking a hit...',
    'MSG_SAVE_BEHAVIOR'             => 'You scored the same as your opponent but your reputation saves you!',
    'MSG_SAME_BEHAVIOR'             => 'You have the same score as your opponent and your reputation is identical to his so you both lose!',
    'MSG_REGISTER_RESULTAT'             => 'Your answer is recorded but your opponent has not yet given his answer!',
];