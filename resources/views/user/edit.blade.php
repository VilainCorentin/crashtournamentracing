@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('ok'))
            <div class="alert alert-dismissible alert-success fade show" role="alert">
                {{ session('ok') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-dismissible alert-danger fade show" role="alert">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if(!empty($errors->first()))
            <div class="alert alert-danger alert-dismissible fade show">
                <span>{{ $errors->first() }}</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-10 mb-3">
                <div class="card">
                    <div class="card-header">{{ __('Informations profil') }}</div>
                    <div class="card-body">

                        <ul class="nav nav-tabs" id="UserTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="user-tab" data-toggle="tab" href="#user" role="tab" aria-controls="user" aria-selected="true">Profil</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="avatar-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="videos" aria-selected="false">Avatar</a>
                            </li>
                        </ul>

                        <br/>

                        <div class="tab-content" id="UserTabContent">

                            <div class="tab-pane fade show active" id="user" role="tabpanel" aria-labelledby="user">

                                <form method="POST" action="{{ route('user.update', $user->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Adresse Mail') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" required autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Pseudo') }}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required>
                                        </div>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Plateforme de jeu') }}</label>
                                        <select class="col-md-6 custom-select" id="plateforme" name="plateforme">
                                            <option value="{{ Auth::user()->plateforme }}">{{ Auth::user()->plateforme }}</option>
                                            <option value="Playstation 4">Playstation 4</option>
                                            <option value="Xbox One">Xbox One</option>
                                            <option value="Switch">Switch</option>
                                        </select>
                                        @error('plateforme')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="form-group row">
                                        <label for="plateforme_name" class="col-md-4 col-form-label text-md-right">{{ __('Nom de joueur sur la plateforme') }}</label>

                                        <div class="col-md-6">
                                            <input id="plateforme_name" type="text" class="form-control" name="plateforme_name" value="{{ Auth::user()->plateforme_name }}" required>
                                        </div>
                                        @error('plateforme_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="form-group row">
                                        <label for="bio" class="col-md-4 col-form-label text-md-right">{{ __('Biographie') }}</label>

                                        <div class="col-md-6">
                                            <textarea id="bio" type="textArea" class="form-control" name="bio" value="{{ Auth::user()->bio }}">{{ Auth::user()->bio }}</textarea>
                                        </div>
                                        @error('bio')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Modifier') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">

                                <div class="form-group">
                                    <label>{{ __('Modification de votre avatar') }}</label>
                                    <form method="post" action="{{ route('save-images') }}" enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                                        @csrf
                                        <div class="dz-message">
                                            <div class="col-xs-8">
                                                <div class="message">
                                                    <p>{{ __('Déposez votre avatar ici ou cliquez') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="preview" style="display: none;">
                                    <div class="dz-preview dz-file-preview">
                                        <div class="dz-image"><img data-dz-thumbnail /></div>
                                        <div class="dz-details">
                                            <div class="dz-size"><span data-dz-size></span></div>
                                            <div class="dz-filename"><span data-dz-name></span></div>
                                        </div>
                                        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                        <div class="dz-error-message"><span data-dz-errormessage></span></div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>

        $('a.form-delete').click((e) => {
            e.preventDefault();
            let href = $(e.currentTarget).attr('href')
            Swal.fire({
                title: '@lang('Vraiment supprimer cette vidéo ?')',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: '@lang('Oui')',
                cancelButtonText: '@lang('Non')'
            }).then((result) => {
                if (result.value) {
                    $("form[action='" + href + "'").submit()
                }
            })
        });

        Dropzone.options.myDropzone = {
            uploadMultiple: false,
            parallelUploads: 1,
            maxFilesize: 3,
            maxFiles: 1,
            previewTemplate: document.querySelector('#preview').innerHTML,
            addRemoveLinks: true,
            acceptedFiles: 'image/*',
            dictInvalidFileType : '{{ __('Type de fichier interdit') }}',
            dictRemoveFile: '{{ __('Supprimer') }}',
            dictFileTooBig: "{{ __("L'image fait plus de 3 Mo") }}",
            timeout: 10000,
            init () {
                const myDropzone = this;
                $.get('{{ route('server-images') }}', data => {
                    $.each(data.image, (key, value) => {
                        const mockFile = {
                            name: value.original,
                            size: value.size,
                            dataURL: '{{ url('avatars') }}' + '/' + value.server
                        };
                        myDropzone.files.push(mockFile);
                        myDropzone.emit("addedfile", mockFile);
                        myDropzone.createThumbnailFromUrl(mockFile,
                            myDropzone.options.thumbnailWidth,
                            myDropzone.options.thumbnailHeight,
                            myDropzone.options.thumbnailMethod, true, (thumbnail) => {
                                myDropzone.emit('thumbnail', mockFile, thumbnail);
                            });
                        myDropzone.emit('complete', mockFile);
                    });
                });
                this.on('addedfile', function(file) {
                    if (myDropzone.files.length > 1) {
                        myDropzone.removeFile(myDropzone.files[0]);
                    }
                });
                this.on("removedfile", file => {
                    $.ajax({
                        method: 'delete',
                        url: '{{ route('destroy-images') }}',
                        data: { name: file.name, _token: $('[name="_token"]').val() }
                    });
                });
            }
        };
    </script>
@endsection
