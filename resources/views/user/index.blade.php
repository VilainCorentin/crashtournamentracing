@extends('layouts.app')

@section('content')
    @if (session('ok'))
        <div class="alert alert-dismissible alert-success fade show" role="alert">
            {{ session('ok') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if(!empty($errors->first()))
        <div class="alert alert-danger alert-dismissible fade show">
            <span>{{ $errors->first() }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="row justify-content-center pb-4">
        <div class="col-md-8">
            <div class="card text-center">
                <div class="card-header">
                    {{ $user->name }}
                </div>
                <div class="card-body">

                    <ul class="nav nav-tabs" id="UserTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="user-tab" data-toggle="tab" href="#user" role="tab" aria-controls="user" aria-selected="true">Profil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="match-tab" data-toggle="tab" href="#match" role="tab" aria-controls="match" aria-selected="false">Matchs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="videos-tab" data-toggle="tab" href="#videos" role="tab" aria-controls="videos" aria-selected="false">Videos</a>
                        </li>
                    </ul>

                    <br/>

                    <div class="tab-content" id="UserTabContent">

                        <div class="tab-pane fade show active" id="user" role="tabpanel" aria-labelledby="user">
                            @if (is_null($user->avatar))
                                <img class="img-fluid rounded-circle"
                                     src="{{ url('avatars') }}/anonymous.png"
                                     alt="Avatar" />
                            @else
                                <img class="img-fluid rounded-circle"
                                     src="{{ url('avatars') }}/{{ $user->avatar }}"
                                     alt="Avatar" />
                            @endif
                            <blockquote class="blockquote">
                                <p class="card-text"><em>- {{ $user->bio }}</em></p>
                            </blockquote>
                            <div class="row col-md-12">
                                <div class="col-md-6" >
                                    <h5 class="card-title bg-primary text-white">{{ __('Plateforme') }} :<br/> {{ $user->plateforme }}</h5>
                                    <h5 class="card-title bg-primary text-white">{{ __('Pseudo InGame') }} :<br/> {{ $user->plateforme_name }}</h5>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="card-title bg-primary text-white">Points :<br/> {{ $user->points }}</h5>
                                    <h5 class="card-title bg-primary text-white">{{ __('Classement') }} {{ $user->plateforme }} :<br/> {{ $user->rank_classement }} / {{ $user->total_player }}</h5>
                                </div>
                            </div>
                            <h5 class="card-title border border-primary">
                                <img class="img-fluid rounded-circle"
                                     src="{{ url('ranks') }}/{{ $user->rank }}.png"
                                     alt="rankAvatar" />
                                </br>
                                {{ __('Rank') }} : {{ __(Config::get('constants.ranking')[$user->rank]) }}
                            </h5>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="match" role="tabpanel" aria-labelledby="match-tab">

                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">{{ __('Résultat') }}</th>
                                    <th scope="col">{{ __('Adversaire') }}</th>
                                    <th scope="col">{{ __('Circuit') }}</th>
                                    <th scope="col">{{ __('Date') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($all_matchs as $match)
                                    <tr
                                            @if ($match['result_int'] === 1)
                                            class="table-success"
                                            @elseif ($match['result_int'] === 0)
                                            class="table-danger"
                                            @elseif ($match['result_int'] === 2)
                                            class="table-primary"
                                            @endif
                                    >
                                        <th scope="row">{{ $match['result'] }}</th>
                                        <td>{{ $match['adversary'] }}</td>
                                        <td>{{ $match['map'] }}</td>
                                        <td>{{ $match['date'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="videos" role="tabpanel" aria-labelledby="videos-tab">

                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">{{ __('Les 3 plus belles courses de') }} {{ $user->name }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($videos as $video)
                                <tr>
                                    <th scope="row">
                                        <hr/>
                                        <h5>
                                            {{ $video->description }}
                                            <a class="btn btn-primary btn-sm" href="{{ route('videoLike', $video->id) }}" role="button" aria-expanded="false">
                                                <i class="fas fa-heart"></i>
                                                <span class="badge badge-light">{{ $video->like }}</span>
                                            </a>
                                        </h5>
                                    </th>
                                </tr>
                                <tr>
                                    <th scope="row" class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="{{ $video->video_url }}" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>

        $("#videos").hide();
        $("#match").hide();

        $("a.nav-link").on('click',function(){
            $(".tab-pane").hide();
            $($(this).attr("href")).show();
        });
    </script>

@endsection
