@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('ok'))
            <div class="alert alert-dismissible alert-success fade show" role="alert">
                {{ session('ok') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-dismissible alert-danger fade show" role="alert">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if(!empty($errors->first()))
            <div class="alert alert-danger alert-dismissible fade show">
                <span>{{ $errors->first() }}</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-10 mb-3">
                <div class="card">
                    <div class="card-header">{{ __('Gérer mes vidéos') }}</div>
                    <div class="card-body">

                        <h5>{{ __("Vos 3 meilleurs courses en video YouTube :") }}</h5>

                        <form method="POST" action="{{ route('video.store', $user->id) }}">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label for="video" class="col-md-4 col-form-label text-md-right">{{ __('URL de la video') }}</label>
                                <div class="col-md-6">
                                    <input id="video" type="text" class="form-control" name="video" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Courte description') }}</label>
                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-7  text-md-right">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Ajouter la video') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr/>

                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">{{ __('Vos meilleurs courses') }}</th>
                                    <th scope="col">{{ __('Lien') }}</th>
                                    <th scope="col">{{ __('Like') }}</th>
                                    <th scope="col">{{ __('Suppression') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($videos as $video)
                                    <tr>
                                        <th scope="row">
                                            {{ $video->description }}
                                        </th>
                                        <td>
                                            <a href="{{ $video->url }}"><i class="fab fa-youtube"></i></a>
                                        </td>
                                        <td>
                                            {{ $video->like }}
                                        </td>
                                        <td>
                                            <a class="form-delete text-danger" href="{{ route('video.destroy', $video->id) }}">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <form action="{{ route('video.destroy', $video->id) }}" method="POST" class="hide">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $('a.form-delete').click((e) => {
            e.preventDefault();
            let href = $(e.currentTarget).attr('href')
            Swal.fire({
                title: '@lang('Vraiment supprimer cette vidéo ?')',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: '@lang('Oui')',
                cancelButtonText: '@lang('Non')'
            }).then((result) => {
                if (result.value) {
                    $("form[action='" + href + "'").submit()
                }
            })
        });
    </script>
@endsection
