<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" href="{{ asset('images/logoCtrvCrash.png') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    <link href="{{ asset('css/classement.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/4d982595a8.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144367999-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-144367999-1');
    </script>
</head>

<body>
<!-- Sidebar -->
<div id="sidebar">
    <div class="sidebar-header">
        <a href="{{ route('home') }}">
            <img src="{{ asset('images/logoCtrvCrash.png') }}" class="card-img" alt="CTR Logo">
        </a>
        <a class="nav-link" style="font-size: 13px;" href="https://twitter.com/CrashTRacing"><i class="fab fa-twitter"></i> @CrashTRacing</a>

        <div class="row justify-content-center">
            <div class="row">
                <div class="col-6">
                    <a class="btn btn-outline-light active" href="#" id="navbarDropdownFlag" role="button sm" aria-expanded="false">
                        <img width="20" height="20" alt="{{ session('locale') }}"
                             src="{!! asset('images/flags/' . session('locale') . '-flag.png') !!}"/>
                    </a>
                </div>
                <div class="col-6">
                    @foreach(config('app.locales') as $locale)
                        @if($locale != session('locale'))
                            <a class="btn btn-outline-light" role="button sm" href="{{ route('language', $locale) }}">
                                <img width="20" height="20" alt="{{ session('locale') }}"
                                     src="{!! asset('images/flags/' . $locale . '-flag.png') !!}"/>
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <ul class="list-unstyled components">

        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-key"></i> @lang('Se connecter')</a>
            </li>
        @else
            <li>
                <a href="#profilSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-user-astronaut"></i> {{ __('Profil') }}</a>
                <ul class="collapse list-unstyled" id="profilSubmenu">
                    <li>
                        <a href="{{ route('user.show', auth()->id()) }}">@lang('Mon Profil')</a>
                    </li>
                    <li>
                        <a href="{{ route('user.edit', Auth::user()) }}">@lang('Modifier mon profil')</a>
                    </li>
                    <li>
                        <a href="{{ route('videoEdit', Auth::user()) }}">@lang('Gérer mes vidéos')</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i>
                            @lang('Déconnexion')
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </li>
        @endguest
        <li>
            <a href="{{ route('research') }}">
                <i class="fas fa-gamepad"></i>
                Matchmaking
            </a>
        </li>
        <li>
            <a href="#classsementSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-trophy"></i> {{ __('Classement') }}
            </a>
            <ul class="collapse list-unstyled" id="classsementSubmenu">
                <li>
                    <a href="{{ route('general', 'Playstation 4') }}">@lang('Playstation 4')</a>
                </li>
                <li>
                    <a href="{{ route('general', 'Switch') }}">@lang('Switch')</a>
                </li>
                <li>
                    <a href="{{ route('general', 'Xbox One') }}">@lang('Xbox One')</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#videoSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-video"></i> {{ __('Vidéos') }}
            </a>
            <ul class="collapse list-unstyled" id="videoSubmenu">
                <li>
                    <a href="{{ route('video.index') }}">@lang('Top')</a>
                </li>
                <li>
                    <a href="{{ route('videoLast') }}">@lang('Les dernières')</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('faq') }}">
                <i class="fas fa-question-circle"></i>
                Faq
            </a>
        </li>
        <li>
            <a href="https://crashteamranking.com/">
                <img src="{{ asset('images/bomb.png') }}" height="25" width="25">
                CTRanking
            </a>
        </li>
        <hr>
        <li>
            <a href="#statplayerSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" style="font-size: 15px">{{ __('Joueurs en recherche') }}</a>
            <ul class="list-unstyled collapse" id="statplayerSubmenu">
                <li>
                    <a href="#" id="cont_ps4">{{ __('Calcul en cours') }}</a>
                </li>
                <li>
                    <a href="#" id="cont_xbox">{{ __('Calcul en cours') }}</a>
                </li>
                <li>
                    <a href="#" id="cont_switch">{{ __('Calcul en cours') }}</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#statMatchSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" style="font-size: 15px">{{ __('Statistique Matchs') }}</a>
            <ul class="list-unstyled collapse" id="statMatchSubmenu">
                <li>
                    <a href="#" id="match_going">{{ __('Calcul en cours') }}</a>
                </li>
                <li>
                    <a href="#" id="match_terminate">{{ __('Calcul en cours') }}</a>
                </li>
            </ul>
        </li>
    </ul>
</div>

<div class="content">
    @yield('content')
</div>

@yield('script')

<script>

    function stats()
    {
        setTimeout(stats, 7000);

        $.ajax({
            url: '/statPlayers/',
            type: 'GET',
            success: function (data) {

                if($.trim(data)){
                    let reps = $.parseJSON(data);

                    $("#cont_ps4").text('PlayStation 4 : ' + reps.count_ps4);
                    $("#cont_xbox").text('Xbox One : ' + reps.count_xbox);
                    $("#cont_switch").text('Switch : ' + reps.count_switch);
                    $("#match_going").text('{{ __("Matchs en cours : ") }}' + reps.count_match_going);
                    $("#match_terminate").text("@lang("Aujourd'hui : ")" + reps.count_match_today);
                }

            },
            error: function (e) {
                console.log(e.responseText);
            }
        });

    }

    stats();

</script>

</body>
</html>
