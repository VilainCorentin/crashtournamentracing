@extends('layouts.app')

@section('content')

    <link href="{{ asset('css/faq.css') }}" rel="stylesheet">

                <div class="tab-content mb-5" id="faq-tab-content">
                    <div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
                        <div class="accordion" id="accordion-tab-1">
                            <div class="card">
                                <div class="card-header" id="accordion-tab-1-heading-1">
                                    <h5>
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-1" aria-expanded="false" aria-controls="accordion-tab-1-content-1">{{ __("Comment que c'est qu'on joue !?!") }}</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="accordion-tab-1-content-1" aria-labelledby="accordion-tab-1-heading-1" data-parent="#accordion-tab-1">
                                    <div class="card-body">
                                        <h5>{{ __('Le commencement :') }}</h5>
                                        <br/>
                                        <p>{{ __('La première chose à faire est de se ') }}<a href="/login" class="btn btn-info btn-sm" tabindex="-1" role="button" aria-disabled="true">{{ __('créer un compte') }}</a>{{ __(' sur Crash Tournament Racing pour que le système puisse vous affecter des statisques de performance. Ces statistiques vont servir à déterminer votre rank et donc quel type de joueur vous allez affronter en matchmaking !') }}</p>
                                        <p>{{ __("Soyez vigilant lors de la création de votre compte à bien mettre dans la case 'Nom de joueur sur la plateforme', l'identifiant qui vous permet de vous ajouter en ami sur votre plateforme.") }}</p>
                                        <p>{{ __("Une fois inscrit et connecté, il ne vous reste plus qu'à vous ") }}<a href="/research" class="btn btn-info btn-sm" tabindex="-1" role="button" aria-disabled="true">{{ __('lancer dans le matchmaking') }}</a>.</p>
                                        <br/>
                                        <h5>{{ __("En recherche d'un match :") }}</h5>
                                        <br/>
                                        <p>{{ __("Cette affichage signifie que vous êtes en recherche de matchmaking et que vous êtes disponible. Si vous quittez la page alors la recherche s'arrête et vous n'êtes plus visible par les autres joueurs.") }}</p>
                                        <div class="text-center">
                                            <img src="{{ asset('images/recherche_screen_faq.png') }}" class="card-img" alt="recherche_screen_faq">
                                        </div>
                                        <br/>
                                        <h5>{{ __('En match :') }}</h5>
                                        <br/>
                                        <p>{{ __("Dès que le matchmaking vous trouve un match sur votre plateforme, vous êtes automatiquement redirigé vers la page du match. Dans cette page, vous y retrouvez le nom du joueur adverse à rentrer en ami ainsi que la carte et les régles de la course. Après chaque course, il est impératif de donner son résultat via les boutons pour cloturer le match.") }}<br/>{{ __("Attention cependant la 'triche' est détéctée et punie ! Chaque joueur a une réputation interne qu'il vaut mieux garder à un bon niveau pour ne pas ête sanctionné.") }}</p>
                                        <div class="text-center">
                                            <img src="{{ asset('images/match_faq.png') }}" class="card-img" alt="recherche_screen_faq">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="accordion-tab-1-heading-2">
                                    <h5>
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-2" aria-expanded="false" aria-controls="accordion-tab-1-content-2">{{ __("Késaco le matchmaking ranked ?") }}</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="accordion-tab-1-content-2" aria-labelledby="accordion-tab-1-heading-2" data-parent="#accordion-tab-1">
                                    <div class="card-body">
                                        <p>{{ __("CrashTournamentRacing intègre un classement Elo (comme pour Lol, CSGO par exemple) qui permet d'évaluer le niveau des joueurs pour faire en sorte de leur mettre un rang et donc de faire jouer les joueurs de même niveau ensemble (de même rang). Si un joueur est globalement meilleur que tous ceux de son rang alors ce joueur va passer au rang du dessus et trouver des joueurs plus forts.") }}<br/>{{ __("Bien sûr, ça marche aussi dans l'autre sens dans le cas d'un joueur 'moins meilleur' que les autres ;).") }}</p>
                                        <p>{{ __("Le rang des joueurs est calculé en fonction de leur niveau général (leur rang), les enchaînements et le ratio de victoires/défaites ainsi que le classement général.") }}</p>
                                        <br/>
                                        <h5>{{ __('Ranking :') }}</h5>
                                        <br/>
                                        <div class="text-center">
                                            <p>{{ __("Le système de rank est divisé en 7 divisions :") }}</p>
                                            <hr>
                                            <p>{{ __("Wumpa") }}</p>
                                            <img src="{{ asset('ranks/0.png') }}">
                                            <hr>
                                            <p>{{ __("Cristal") }}</p>
                                            <img src="{{ asset('ranks/1.png') }}">
                                            <hr>
                                            <p>{{ __("Gemme") }}</p>
                                            <img src="{{ asset('ranks/2.png') }}">
                                            <hr>
                                            <p>{{ __("Gemme Colorée") }}</p>
                                            <img src="{{ asset('ranks/3.png') }}">
                                            <hr>
                                            <p>{{ __("Relique Saphir") }}</p>
                                            <img src="{{ asset('ranks/4.png') }}">
                                            <hr>
                                            <p>{{ __("Relique Or") }}</p>
                                            <img src="{{ asset('ranks/5.png') }}">
                                            <hr>
                                            <p>{{ __("Relique Platine") }}</p>
                                            <img src="{{ asset('ranks/6.png') }}">
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="accordion-tab-1-heading-3">
                                    <h5>
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-3" aria-expanded="false" aria-controls="accordion-tab-1-content-3">{{ __("Comment savoir qui est en ligne ?") }}</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="accordion-tab-1-content-3" aria-labelledby="accordion-tab-1-heading-3" data-parent="#accordion-tab-1">
                                    <div class="card-body">
                                        <p>{{ __("Il est possible de savoir qui est en recherche de match et sur quelle plateforme en regardant la partie 'Joueurs en recherche' du menu qui indique par plateforme le nombre de joueurs actuellement en recherche de match.") }}</p>
                                        <p>{{ __("Vous pouvez aussi voir le nombre de matchs en cours ainsi que le nombre de matchs terminés dans la journée.") }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
