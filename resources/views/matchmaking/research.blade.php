@extends('layouts.app')

@section('content')
<div class="container mb-5">
    <div class="crash-title-container"><div class="crash-title">{{ __("Recherche d'un adversaire à votre niveau en cours...") }}</div></div>
    <div class="loader-container">
        <div id="circle">
            <div class="loader">
                <div class="loader">
                    <div class="loader">
                        <div class="loader">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    let iter = 0;

    function research(iter)
    {
        setTimeout(function() {research(iter)}, 5000);

        iter = iter + 1;

        $.ajax({
            url: '/researchMatchmaking/' + "{{ Auth::user()->id }}" + '/' + iter,
            type: 'GET',
            success: function (data) {

                if($.trim(data)){

                    console.log(data);
                    let reps = $.parseJSON(data);
                    document.location.href='/displayMatch/' + reps.user + '/' + reps.user_adv + '/' + reps.match + '/' + reps.msg
                }

            },
            error: function (e) {
                console.log(e.responseText);
            }
        });

        return iter;
    }

    iter = research(iter);

</script>

@endsection
