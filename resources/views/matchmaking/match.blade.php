@extends('layouts.app')

@section('content')
    @if($msg == 1)
        <div class="alert alert-danger" role="alert">
            {{__('Vous avez quitté ce match sans donner de résultat, indiquez le avec les boutons en bas de page pour pouvoir reprendre le matchmaking.')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="row justify-content-center mb-5">
        <div class="col-md-8">
            <div class="card text-center">
                <div class="card-header">
                    Match : {{ $user_main->plateforme_name }} VS {{ $user_adv->plateforme_name }}
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{ __('Adversaire à ajouter dans vos amis sur votre') }} {{ $user_adv->plateforme }} :</h5>
                    <h4 class="card-title border border-success rounded">{{ $user_adv->plateforme_name}}</h4>
                    <hr>
                    <h4 class="card-title">{{ __('Circuit') }} :</h4>
                    <h4 class="card-title border border-info rounded">{{ $match->map_string }}</h4>
                    <hr>
                    <h4 class="card-title">{{ __('Règles') }} :</h4>
                    <p class="card-text text-muted border border-danger rounded">- {{ __('Course sans IA.') }}</p>
                    <p class="card-text text-muted border border-danger rounded">- {{ __('Course en 3 tours.') }}</p>
                </div>
                {{ __('Merci pour votre fairplay.') }}
                <div class="card-footer">
                    <h4>{{ __('Résultat') }} :</h4>
                    <div class="btn-group" role="group" aria-label="Resultat">
                        <form class="mr-1" action="/validateMatch/{{ $user_main->id }}/{{ $match->id }}/1">
                            <input type="submit" class="btn btn-success" value="{{ __("Gagné") }}" />
                        </form>
                        <form class="mr-1 ml-1" action="/validateMatch/{{ $user_main->id }}/{{ $match->id }}/2">
                            <input type="submit" class="btn btn-warning" value="{{ __("Match nul") }}" />
                        </form>
                        <form class="ml-1" action="/validateMatch/{{ $user_main->id }}/{{ $match->id }}/0">
                            <input type="submit" class="btn btn-danger" value="{{ __("Perdu") }}" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
