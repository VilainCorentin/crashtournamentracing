@extends('layouts.app')

@section('content')
    @if (session('ok'))
        <div class="alert alert-dismissible alert-success fade show" role="alert">
            {{ session('ok') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="container">
        <table class="table" style="background-color: rgba(255,255,255) !important;">
            <thead class="thead-dark">
            <tr>
                <th scope="col">{{ __($title) }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($videos as $video)
                <tr>
                    <th scope="row" class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{ $video->video_url }}" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </th>
                </tr>
                <tr>
                    <th scope="row">
                        <div class="row align-self-center">
                            <div class="col-md-6 m-auto">
                                <h5 class="text-center">
                                    {{ $video->description }}
                                    <br/>
                                    <a class="btn btn-primary btn-sm" href="{{ route('videoLike', $video->id) }}" role="button" aria-expanded="false">
                                        <i class="fas fa-heart"></i>
                                        <span class="badge badge-light">{{ $video->like }}</span>
                                    </a>
                                </h5>
                            </div>
                            <div class="col-md-6 m-auto">
                                <a href="{{ route('user.show', $video->user_id) }}">
                                    <h5 class="text-center">
                                        <div class="m-auto">
                                            @if (is_null($video->user_avatar))
                                                <img class="img-responsive rounded-circle"
                                                     src="{{ url('avatars') }}/anonymous.png"
                                                     alt="Avatar"
                                                     height="42" width="42"/>
                                            @else
                                                <img class="img-responsive rounded-circle"
                                                     src="{{ url('avatars') }}/{{ $video->user_avatar }}"
                                                     alt="Avatar"
                                                     height="42" width="42"/>
                                            @endif
                                        </div>
                                        {{ $video->user_name }}
                                    </h5>
                                </a>
                            </div>
                        </div>
                        <hr/>
                    </th>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
