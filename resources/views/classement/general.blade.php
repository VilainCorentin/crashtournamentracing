@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="classement-border">
                <div id="triangle-bottomright"></div>
                <div id="classement-title-container">
                    <div id="classement-title">
                        {{ __('Top 50 classement') }} {{ $platform }}
                    </div>
                </div>
                <div id="triangle-bottomleft"></div>
            </div>
            <div class="table-responsive">
                <table id="classement-table" class="table table-light">
                    <thead>
                    <tr>
                        <th scope="col">#{{ __('Placement') }}</th>
                        <th scope="col">{{ __('Joueur') }}</th>
                        <th scope="col">{{ __('Rank') }}</th>
                        <th scope="col">{{ __('Points') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td class="text-nowrap">
                                <a href="{{ route('user.show', $user->id) }}">
                                    @if (is_null($user->avatar))
                                        <img class="img-fluid rounded-circle"
                                             src="{{ url('avatars') }}/anonymous.png"
                                             alt="Avatar"
                                             height="42" width="42"/>
                                    @else
                                        <img class="img-fluid rounded-circle"
                                             src="{{ url('avatars') }}/{{ $user->avatar }}"
                                             alt="Avatar"
                                             height="42" width="42"/>
                                    @endif
                                    {{ $user->name }}
                                </a>
                            </td>
                            <td data-toggle="tooltip" data-placement="top" title="{{ __(Config::get('constants.ranking')[$user->rank]) }}">
                                <img class="img-fluid rounded-circle"
                                     src="{{ url('ranks') }}/{{ $user->rank }}.png"
                                     alt="Avatar"
                                     height="42" width="42"/>
                            </td>
                            <td class="text-nowrap">{{ $user->points }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
