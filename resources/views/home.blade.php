@extends('layouts.app')

@section('content')
    <div class="alert alert-info" role="alert">
        {{ __("Il est conseillé de venir faire du matchmaking entre 18h et 23h, on a qu'une faible fréquentation pour le moment.") }} {{ __("N'hésitez pas à lire la ") }} <a href="/faq" class="btn btn-info btn-sm" tabindex="-1" role="button" aria-disabled="true">Faq</a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="alert alert-info" role="alert">
        {{ __("CrashTournamentRacing recherche activement des partenaires pour organiser des sessions de matchmaking avec la communauté !") }} <br/> {{ __("Vous pouvez me contacter sur mon twitter") }} <a href="https://twitter.com/CrashTRacing">@CrashTRacing <i class="fab fa-twitter"></i></a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @if(session('bad'))
        <div class="alert alert-danger" role="alert">
            {{ session('bad') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if(session('good'))
        <div class="alert alert-success" role="alert">
            {{ session('good') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <div class="text-center mt-5">
                            <img src="{{ asset('images/logoCtrvCrash.png') }}" class="card-img" alt="CTR Logo">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">{{ __("Qu'est ce que Crash Tournament Racing ?!") }}</h5>
                            <p class="card-text text-muted">{{ __("Crash Tournament Racing est un système de matchmaking en ligne pour Crash Team Racing Nitro Fueled qui permet de se mesurer à des adversaires de son niveau et rendre le Online du jeu un peu plus piquant !") }}</p>
                            <p class="card-text text-muted">{{ __("Le matchmaking comprends un classement par Elo, plus vous gagnez des matchs et plus vous allez jouer contre des joueurs coriaces. Attention ça marche aussi dans l'autre sens si vous enchaînez les défaites...") }}</p>
                            <p class="card-text text-muted">{{ __("Attention, chaque joueur a une jauge de réputation qui varie selon les comportements. Par exemple, si vous déclarez vos matchs perdus comme gagnés... Cela ne sera pas sans conséquences !") }}</p>
                            <p class="card-text text-muted">{{ __("Il ne vous reste plus qu'à vous ") }}<a href="/login" class="btn btn-info btn-sm" tabindex="-1" role="button" aria-disabled="true">{{ __("connecter") }}</a>{{ __(" et vous lancer dans le ") }}<a href="/research" class="btn btn-info btn-sm" tabindex="-1" role="button" aria-disabled="true">matchmaking</a><br/>{{ __("En cas de questions, n'hésitez pas à vous rendre dans la ") }}<a href="/faq" class="btn btn-info btn-sm" tabindex="-1" role="button" aria-disabled="true">Faq</a>{{ __(" pour en savoir plus.") }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row justify-content-center mb-4">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-7">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">{{ __('Les 10 derniers matchs') }}</th>
                                <th scope="col">{{ __('Circuit') }}</th>
                                <th scope="col">{{ __('Plateforme') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($all_matchs as $match)
                                <tr
                                        @if ($match['result_int'] === 1)
                                        class="table-success"
                                        @elseif ($match['result_int'] === 0)
                                        class="table-danger"
                                        @else
                                        class="table-primary"
                                        @endif
                                >
                                    <th scope="row" class="text-nowrap">{{ $match['result'] }} {{ __('de') }} {{ $match['name1'] }} {{ __('contre') }} {{ $match['name2'] }}</th>
                                    <td class="text-nowrap">{{ $match['map'] }}</td>
                                    <td class="text-nowrap">{{ $match['plateforme'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="table-responsive">
                        <table id="classement-table" class="table table-light">
                            <thead>
                            <tr>
                                <th scope="col">#{{ __('Top 10') }}</th>
                                <th scope="col">{{ __('Rank') }}</th>
                                <th scope="col">{{ __('Points') }}</th>
                                <th scope="col">{{ __('Plateforme') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <th class="text-nowrap">
                                        <a href="{{ route('user.show', $user->id) }}">
                                            <div style="height:100%;width:100%">
                                                @if (is_null($user->avatar))
                                                    <img class="img-fluid rounded-circle"
                                                         src="{{ url('avatars') }}/anonymous.png"
                                                         alt="Avatar"
                                                         height="42" width="42"/>
                                                @else
                                                    <img class="img-fluid rounded-circle"
                                                         src="{{ url('avatars') }}/{{ $user->avatar }}"
                                                         alt="Avatar"
                                                         height="42" width="42"/>
                                                @endif
                                                {{ $user->name }}
                                            </div>
                                        </a>
                                    </th>
                                    <td data-toggle="tooltip" data-placement="top" title="{{ __(Config::get('constants.ranking')[$user->rank]) }}">
                                        <img class="img-fluid rounded-circle"
                                             src="{{ url('ranks') }}/{{ $user->rank }}.png"
                                             alt="Avatar"
                                             height="42" width="42"/>
                                    </td>
                                    <td class="text-nowrap">{{ $user->points }}</td>
                                    <td class="text-nowrap">{{ $user->plateforme }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
