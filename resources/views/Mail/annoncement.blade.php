<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>
<body>
<img src="https://www.crashtournamentracing.com/images/logoCtrvCrash.png" height="100" alt="CTR Logo">
<h2>[FR] Quelques nouvelles de CrashTournamentRacing !</h2>
<p>Salut {{ $user->name }}, ça fait un petit moment qu'on ne t'a pas vu. ;)</p>
<p>Pour le lancement des nouvelles divisions, on organise une session de matchmaking ce soir (18/08/2019) entre 20h et 23h toute plateforme confondue.</p>
<p>Pour information, voici les différentes nouveautés du site :</p>
<ul>
    <li><strong>7 Divisions à gravir :</strong></li>
    <img src="https://www.crashtournamentracing.com/ranks/0.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/1.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/2.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/3.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/4.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/5.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/6.png" height="32" width="32" alt="division">
    <p>Plus d'informations dans la <a href="https://www.crashtournamentracing.com/faq" class="btn btn-info btn-sm" tabindex="-1" role="button" aria-disabled="true">Faq</a></p>
    <li><strong>Un système de classement des meilleurs vidéos de courses :</strong></li>
    <p>Vous pouvez maintenant partager vos meilleurs vidéos de courses sur le site !</p>
</ul>
<h3>Si t'es chaud pour participer à la session de ce soir, oublie pas de ramener tes potes qui seraient aussi intéressés ! Plus y aura de monde, plus ça sera fun !</h3>
<h4>Rendez vous sur <a href="https://www.crashtournamentracing.com" tabindex="-1" role="button" aria-disabled="true">CrashTournamentRacing</a></h4>
<h4>Pour rappel, CrashTournamentRacing est un projet pour la communauté par la communauté et bien sûr à but non lucratif ! On est inutile sans vous ! :p</h4>
<h5>L'équipe de CrashTournamentRacing</h5>
<p>Ps : Désolé pour ceux qui ont reçu 2 fois le mail. X(</p>
<hr/>
<br/>
<h2>[EN] Some news from CrashTournamentRacing!</h2>
<p>Hi {{ $user->name }}, It's been a while since we've seen you. ;)</p>
<p>For the launch of the new divisions, a matchmaking session is organized tonight (18/08/2019) between 8pm and 11pm all platforms.</p>
<p>For information, here are the different new features of the site:</p>
<ul>
    <li><strong>7 Divisions to be climbed:</strong></li>
    <img src="https://www.crashtournamentracing.com/ranks/0.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/1.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/2.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/3.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/4.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/5.png" height="32" width="32" alt="division"> => <img src="https://www.crashtournamentracing.com/ranks/6.png" height="32" width="32" alt="division">
    <p>More information in the <a href="https://www.crashtournamentracing.com/faq" class="btn btn-info btn-sm" tabindex="-1" role="button" aria-disabled="true">Faq</a></p>
    <li><strong>A system for ranking the best race videos:</strong></li>
    <p>You can now share your best racing videos on the site!</p>
</ul>
<h3>If you're hot to attend tonight's session, don't forget to bring your friends who would also be interested! The more people there are, the more fun it'll be!</h3>
<h4>Visit us at <a href="https://www.crashtournamentracing.com" tabindex="-1" role="button" aria-disabled="true">CrashTournamentRacing</a></h4>
<h4>As a reminder, CrashTournamentRacing is a community project by the community and of course non-profit! We're useless without you! :p</h4>
<h5>The CrashTournamentRacing team</h5>
<p>Ps: Sorry for those who received the email twice. X(</p>
</body>
</html>
